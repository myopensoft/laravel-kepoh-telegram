# laravel-kepoh-telegram

[![Latest Version on Packagist](https://img.shields.io/packagist/v/myopensoft/laravel-kepoh-telegram.svg?style=flat-square)](https://packagist.org/packages/myopensoft/laravel-kepoh-telegram)
[![Total Downloads](https://img.shields.io/packagist/dt/myopensoft/laravel-kepoh-telegram.svg?style=flat-square)](https://packagist.org/packages/myopensoft/laravel-kepoh-telegram)

To send error that been triggered by exception to Telegram public channels for internal error logging purpose.

## Installation 

You can install the package via composer:

```bash
composer require myopensoft/laravel-kepoh-telegram
```

You can publish the config file with:
```bash
php artisan vendor:publish --provider="Myopensoft\KepohTelegram\KepohTelegramServiceProvider" --tag="kepoh-telegram-config"
```

This is the contents of the published config file:

```php
return [
    'telegram' => [
        'enable' => env('KEPOH_TELEGRAM_ENABLE', false),
        'bot_token' => env('KEPOH_TELEGRAM_BOT_TOKEN'),
        'chat_id' => env('KEPOH_TELEGRAM_CHAT_ID'),
        'dont_report' => [
            \Illuminate\Auth\AuthenticationException::class,
            \Illuminate\Auth\Access\AuthorizationException::class,
            \Illuminate\Session\TokenMismatchException::class,
            \Illuminate\Validation\ValidationException::class,
        ]
    ]
];
```

## Usage

Put into Handler.php
```php
public function report(Throwable $exception)
{
    // ...
    
    KepohTelegram::report($exception);

    parent::report($exception);
}
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](.github/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Muhammad Syafiq Bin Zainuddin](https://github.com/lomotech)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
