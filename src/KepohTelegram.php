<?php

namespace Myopensoft\KepohTelegram;

use Illuminate\Support\Arr;
use Request;

class KepohTelegram
{
    /**
     * Report and log an exception to Telegram.
     * @param \Throwable $exception
     */
    public static function report($exception)
    {
        if (config('kepoh.telegram.enable') && !self::ensureToReportByErrorType($exception)) {
            self::send('BUG', $exception->getMessage(), $exception->getFile(), $exception->getLine(),
                Request::url(), Request::method(), json_encode(Request::all()), url()->previous());
        }
    }

    /**
     * Check if the error is needed to be reported
     * @param \Throwable $exception
     * @return bool
     */
    public static function ensureToReportByErrorType($exception): bool
    {
        return !is_null(Arr::first(config('kepoh.telegram.dont_report'),
            function ($type) use ($exception) {
                return $exception instanceof $type;
            }));
    }

    /**
     * Send status to telegram bot
     *
     * @param string $type
     * @param string $message
     * @param string $file
     * @param string $line
     * @param string $url
     * @param string $request_type
     * @param string $param
     * @param string $previous_url
     * @return void
     */
    public static function send(string $type, string $message, string $file, string $line, string $url,
                                string $request_type, string $param, string $previous_url)
    {
        $data = [
            'chat_id' => config('kepoh.telegram.chat_id'),
            'text' => '[' . config('app.url') . '] ' . ' ' . $type . ' ' . date('Y-m-d H:i:s') . PHP_EOL
                . 'MSG ' . json_encode(mb_substr($message, 0, 3900)) // limit to first 3900 char
                . "FILE " . $file . PHP_EOL // limit to first 3900 char
                . "LN " . $line . PHP_EOL// limit to first 3900 char
                . "URL " . $url . PHP_EOL
                . 'REQ ' . $request_type . PHP_EOL
                . "PAR " . $param . PHP_EOL
                . "PRV " . $previous_url . PHP_EOL
                . "UID " . (auth()->user()->id ?? '-') . PHP_EOL,
        ];

        file_get_contents("https://api.telegram.org/bot" . config('kepoh.telegram.bot_token')
            . "/sendMessage?" . http_build_query($data));

        return;
    }
}
