# Changelog

All notable changes to `laravel-kepoh-telegram` will be documented in this file.

## 1.0.1 - 2021-03-25

- Bug repairs: url, previous_url, param jumble-up each other.
- Add spartie/laravel-ray for debugging purpose.

## 1.0.0 - 2021-03-24

- Initial release
